var Put = require('bufferput');
var buffertools = require('buffertools');
var hex = function(hex) {return new Buffer(hex, 'hex');};
var hexReverse = function(hex) {return buffertools.reverse(new Buffer(hex, 'hex'));};

exports.livenet = {
  name: 'livenet',
  addressVersion: 0x28, // base58.h PUBKEY_ADDRESS //ok
  // magic: hex('cdf1c0ef'), //testnet
  magic: hex('cefbfadb'), // main.cpp pchMessageStart //ok
  genesisBlock: {
    height: 0, //ok
    nonce: 640936, //ok
    version: 1, //ok
    hash: hexReverse('000005510a96f9c91fc4767e9fbb0c295abca7ff25d8dd286b3dba3a5975f374'), //ok
    prev_hash: hex('0000000000000000000000000000000000000000000000000000000000000001'), //?????
    // prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1401695545, //ok
    merkle_root: hex('33feeb335c78d716685c97dcb4ef9872f9ffaf0c15efc9e53fd1580c8d241024'), //ok
    bits: '1e0fffff' //ok
  },
  checkpoints: [], // need to put checkpoint blocks here
  addressPubkey: 28, // base58.h PUBKEY_ADDRESS //ok
  addressScript: 8, // base58.h SCRIPT_ADDRESS //ok
  keySecret: 128,
};
